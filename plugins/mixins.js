import Vue from 'vue'

const mixin = {
	methods: {
		transformToFormData( form, fileKeys, method = '' ){
			// Initializes the form dat object that we will be appending to
			let formData = new FormData();

			// Iterates over all elements in the form. Adds them to the
			// form data object. If the value is a file, we append the
			// file to the form data.
			for (let [key, value] of Object.entries(form)) {
				if( fileKeys.indexOf( key ) > -1 ){
					if( value !== '' ){
						formData.append( key, value[0] );
					}
				}else{
					// Booleans don't send as a true boolean through form data.
					// We send it as a 1 or 0 to make for easier processing on the
					// backend
					if( typeof value === "boolean" ){
						value = value ? 1 : 0;
					}
					formData.append( key, value );
				}
			}

			// If we have a method we need to send as, we append it here
			if( method !== '' ){
				formData.append( '_method', method );
			}

			return formData;
		},
	}
}

Vue.mixin(mixin)
