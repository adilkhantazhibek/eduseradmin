export default function({ $auth, $axios, $store, redirect }) {
	$axios.onError(error => {
		if (error.response.status === 500) {
			$store.dispatch('error/setErrors', error.response.error).then(r => console.log('asd'))
		}
		return Promise.reject(error);
	});
}
