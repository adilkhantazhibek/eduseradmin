export default {
	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "admin-eduser",
		htmlAttrs: {
			lang: "en"
		},
		meta: [
			{charset: "utf-8"},
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1"
			},
			{hid: "description", name: "description", content: ""},
			{name: "format-detection", content: "telephone=no"}
		],
		link: [{rel: "icon", type: "image/x-icon", href: "/favicon.ico"}]
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ["@/assets/css/app.sass"],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [
		{src: "@/plugins/v-select", mode: "client"},
		{src: "@/plugins/quill-editor", mode: "client"},
		{src: "@/plugins/draggable", mode: "client"},
		{src: "@/plugins/flags"},
		{src: "@/plugins/vuelidate"},
		{src: "@/plugins/mixins"},
		{src: "@/plugins/pagination"},
	],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	server: {
		port: 4200,
	},

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		"@nuxtjs/svg",
		'@nuxtjs/dotenv'
	],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://go.nuxtjs.dev/axios
		"@nuxtjs/axios",
		'@nuxtjs/auth-next',
		'vue-wait/nuxt',
		// "bootstrap-vue/nuxt",
	],

	// Optionally passing options in module top level configuration
	wait: {useVuex: true},

	// Axios module configuration: https://go.nuxtjs.dev/config-axios
	axios: {
		baseURL: "https://api.eduser.kz/",
		proxyHeaders: false,
		credentials: false
	},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {},

	//Auth
	auth: {
		redirect: {
			login: '/',
			logout: '/',
			callback: '/',
			home: '/'
		},
		strategies: {
			local: {
				token: {
					property: 'access_token',
					// global: true,
					// required: true,
					type: 'Bearer',
					maxAge: 1800
				},
				refreshToken: {
					property: 'refresh_token',
					maxAge: 60 * 60 * 24 * 30
				},
				user: {
					property: 'user',
					autoFetch: false
				},
				endpoints: {
					login: {
						url: 'https://api.eduser.kz/auth-service/auth/login',
						method: 'post',
						userinfo_endpoint: false
					},
					user: {url: 'https://api.eduser.kz/auth-service/users/my', method: 'get'}
				}
			}
		}
	}
};
