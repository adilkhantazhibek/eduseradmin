export const state = () => ({
	data: {
		subjects: [],
		educationalDirections: [],
		universities: [],
		speciality_universities: [],
		quotaModalIndex: null,
		quotas: [
			{key: "serpinQuota", val: "Серпін"},
			{key: "ruralQuota", val: "Сельская квота"},
			{key: "targetedGrantQuota", val: "Целевой грант"},
			{key: "familyQuota", val: "Многодетная семья"},
			{key: "pedagogicalQuota", val: "Педагогическая квота"},
			{key: "disabilityQuota", val: "Инвалидность. Неполная семья. Детей-сирот и детей"},
		]
	}
})

export const getters = {
	GET: (state) => (payload) => state.data[payload],
	FIND_SPECIALITY_UNIVERSITY: (state) => (payload) => {
		return state.data.speciality_universities.find(item => item.universityId === payload)
	},
	FIND_INDEX_SPECIALITY_UNIVERSITY: state => payload => {
		return state.data.speciality_universities.findIndex(item => item.universityId === payload)
	},
}

export const mutations = {
	SET: (state, payload) => {
		state.data[payload.name] = payload.data
	},
	SET_SPECIALITY_UNIVERSITY: (state, payload) => {
		state.data.speciality_universities.push(payload)
	},
	UPDATE_SPECIALITY_UNIVERSITY_THRESHOLD_SCORE: (state, payload) => {
		state.data.speciality_universities[payload.index].thresholdScore = payload.data
	},
	SET_SPECIALITY_UNIVERSITY_QUOTA_PROBABILITIES: (state, payload) => {
		state.data.speciality_universities[payload.speciality_university_index][payload.quota].quotaProbabilities[payload.index] = payload.data
	},
	UPDATE_SPECIALITY_UNIVERSITY_QUOTA_PROBABILITIES: (state, payload) => {
		state.data.speciality_universities[payload.speciality_university_index][payload.quota].quotaProbabilities[payload.index].score = payload.data
	},
	CHANGE_SPECIALITY_UNIVERSITY_QUOTA: (state, payload) => {
		state.data.speciality_universities[payload.index][payload.field].status = payload.data
	},
	TOGGLE_SPECIALITY_UNIVERSITY_QUOTA: (state, payload) => {
		state.data.speciality_universities[payload.index][payload.field].status = !state.data.speciality_universities[payload.index][payload.field].status
	},
	TOGGLE_SPECIALITY_UNIVERSITY: (state, payload) => {
		state.data.speciality_universities[payload.index].isActive = !state.data.speciality_universities[payload.index].isActive
	},
}

export const actions = {

	async getSpecialityUniversities({commit, dispatch, getters}, payload) {
		await this.$axios.get('handbook-service/univer-spec-quota/speciality/' + payload)
			.then(response => {
				let speciality_universities = response.data
				if (response.data) {
					speciality_universities.forEach((sp) => {
						dispatch('setSpecialityUniversities', sp)
					})
				}
			})
	},

	setSpecialityUniversities({commit, getters, state}, payload) {
		let speciality_university_index = getters.FIND_INDEX_SPECIALITY_UNIVERSITY(payload.universityId);
		let speciality_university_response = payload;
		let speciality_university = getters.FIND_SPECIALITY_UNIVERSITY(payload.universityId);

		if (speciality_university) {
			if (speciality_university_response.isActive) {
				commit("TOGGLE_SPECIALITY_UNIVERSITY", {index: speciality_university_index})
			}

			commit("UPDATE_SPECIALITY_UNIVERSITY_THRESHOLD_SCORE", {
				index: speciality_university_index,
				data: speciality_university_response.thresholdScore
			})

			state.data.quotas.forEach(quota => {
				if (speciality_university_response[quota.key] && speciality_university_response[quota.key].quotaProbabilities.length > 0) {
					speciality_university_response[quota.key].quotaProbabilities.forEach((dqp, index) => {
						if (dqp.score > 0) {
							commit("CHANGE_SPECIALITY_UNIVERSITY_QUOTA", {index: speciality_university_index, field: quota.key, data: true})
						}

						commit('SET_SPECIALITY_UNIVERSITY_QUOTA_PROBABILITIES', {
							speciality_university_index: speciality_university_index,
							index: index,
							quota: quota.key,
							data: {probability: dqp.probability, score: dqp.score}
						})
					})
				}
			})
		}
	},
	async saveSpecialityUniversities({commit, getters, state}) {
		// let speciality_university = state.data.speciality_universities[state.data.quotaModalIndex];
		await this.$axios.post('handbook-service/univer-spec-quota', state.data.speciality_universities)
	},
	async getUniversities({commit}) {
		await this.$axios.get('handbook-service/universities')
			.then(response => {
				commit('SET', {name: 'universities', data: response.data})
			})
	},
	async getSubjects({commit}) {
		await this.$axios.get('handbook-service/subjects')
			.then(response => {
				commit('SET', {name: 'subjects', data: response.data})
			});
	},
	async getEducationalDirections({commit}) {
		await this.$axios.get('handbook-service/educational-direction')
			.then(response => {
				commit('SET', {name: 'educationalDirections', data: response.data})
			});
	},
	async setDefaultSpecialityUniversities({commit, getters, state}, payload) {
		commit('SET', {name: 'speciality_universities', data: []})
		let universities = getters.GET('universities')
		universities.forEach((university) => {

			let defQuotaProbabilities = [50, 60, 70, 80, 90, 100]
			let defQuotas = {}

			state.data.quotas.forEach(quota => {
				let quotaProbabilities = []

				defQuotaProbabilities.forEach(dqp => {
					let qp = {
						probability: dqp,
						score: null
					}

					quotaProbabilities.push(qp)
				})


				defQuotas = Object.assign({}, defQuotas, {
					[quota.key]: {
						quotaProbabilities: quotaProbabilities,
						status: false
					}
				})
			})

			let sp = Object.assign(
				{},
				{
					universityId: university.id,
					specialityId: payload,
					isActive: false,
				},
				defQuotas)

			commit('SET_SPECIALITY_UNIVERSITY', sp)
		})
	}
}
