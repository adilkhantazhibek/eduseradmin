export const state = () => ({
	errors: null,
	status: false
})

export const getters = {
	errors: (state) => () => state.errors,
	status: (state) => () => state.status,
}

export const mutations = {
	SET_ERROR: (state, error) => {
		state.errors.push(error)
	},
	TOGGLE_STATUS: (state) => {
		state.status = !state.status
	},
	ENABLE_STATUS: (state) => {
		state.status = true
	},
	DISABLE_STATUS: (state) => {
		state.status = false
	},
}

export const actions = {
	async setErrors({commit}, errors) {
		errors.forEach(error => {
			commit('SET_ERROR', error)
		})

		commit('TOGGLE_STATUS', true)
	},
}
