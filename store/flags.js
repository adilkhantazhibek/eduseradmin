export const state = () => ({
    data: {},
	removeLink: null
})

export const getters = {
    CHECK: (state) => (payload) => {
        return Boolean(state.data[payload])
    },
    GET: (state) => (payload) => state.data[payload],
	getRemoveLink: (state) => state.removeLink
}

export const mutations = {
    TOGGLE: (state, payload) => {
        state.data = Object.assign({}, state.data, {[payload]: !Boolean(state.data[payload])})
    },
    SET: (state, payload) => {
        state.data = Object.assign({}, state.data, payload)
    },
	SET_REMOVE_LINK: (state, payload) => {
        state.removeLink = payload
    },
}

export const actions = {
    toggle: ({ commit }, payload) => {
        commit('TOGGLE', payload)
    },
    set: ({ commit }, payload) => {
        commit('SET', payload)
    }
}
